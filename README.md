# Shiftumi 

This is a software for Hydrogen-Nitrogen RMN data analysis, issued during a titration experiment.

We use standard Python3 modules for data-science (numpy and pandas), and Qt for the graphical user interface.

## Features

This software is under ongoing development, it currently features :

- Visualizing chemical shifts at each titration step
- Identifying residues having highest chemical shift intensity (i.e higher than user-defined threshold)
- Visualizing chemical shift for a subset of selected residues 
- Setting up a the titration protocole, allowing computation and visualization of volumes, concentrations and concentration ratio at each step.

## Installation

Clone the repository
```bash
git clone https://gitlab.com/lsdch/shiftumi.git
cd shiftumi
```
... or download a snapshot [here](https://gitlab.com/lsdch/shiftumi/-/archive/master/shiftumi-master.zip).

Install python dependencies :

- user scope 
    ```bash
    pip3 install --user -r requirements.txt
    ```

- in a virtualenv
    ```bash
    mkdir .venv                 # virtualenv directory
    virtualenv .venv -p python3 # create virtualenv
    source .venv/bin/activate   # activate virtual environment

    # Python modules are installed in current virtualenv
    pip3 install -r requirements.txt

    # Exiting the current virtualenv is :
    source .venv/bin/deactivate
    ```

## Start Shiftumi

```bash
python3 main.py
```

## Upgrading

1. Pull latest version
    ```bash
    git pull
    ```
2. Update Python dependencies
    ```bash
    source .venv/bin/activate   # if you are using virtualenv
    pip3 install --upgrade [--user] -r requirements.txt
    ```
3. Your version is up to date.

## Roadmap

- v1.0
    - Fitting chemical shift intensities for a given residue to a known model
    - Visualizing titration curve issued by these models
    - Loading PDB/Fasta files to include sequence information + 2D structure
    - 3D model visualization


