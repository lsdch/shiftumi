from PyQt5.QtWidgets import QStyledItemDelegate, QDoubleSpinBox
from PyQt5.QtCore import Qt, QEvent
from PyQt5.QtGui import QColor


class SpinBoxDelegate(QStyledItemDelegate):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.hoverIndex = None

    def createEditor(self, parent, option, index):
        editor = QDoubleSpinBox(parent)
        editor.setMinimum(1)
        editor.setFrame(False)
        editor.setAlignment(Qt.AlignHCenter)
        self.editor = editor
        return editor

    def setEditorData(self, editor, index):
        value = index.model().data(index, Qt.EditRole).value()
        editor.setValue(value)

    def setModelData(self, editor, model, index):
        editor.interpretText()
        value = editor.value()
        model.setData(index, value, Qt.EditRole)

    def commitAndClose(self):
        self.commitData.emit(self.editor)
        self.closeEditor.emit(self.editor, self.EditNextItem)

    def eventFilter(self, obj, event):
        if event.type() == QEvent.KeyPress:
            if event.key() in [Qt.Key_Return, Qt.Key_Enter]:
                self.commitAndClose()
                return True
            if event.key() == Qt.Key_Escape:
                self.closeEditor.emit(self.editor)
        return False

    def paint(self, painter, option, index):
        if index == self.hoverIndex and index.row() != 0:
            painter.fillRect(option.rect, QColor(100, 200, 255, 100))
        super().paint(painter, option, index)

    def hover(self, index):
        self.hoverIndex = index
