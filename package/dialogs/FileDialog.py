import qtawesome as qta
from PyQt5 import uic
from PyQt5.QtCore import QDir, QFile, QStandardPaths, Qt, pyqtSlot
from PyQt5.QtWidgets import QHeaderView, QDialogButtonBox, QDialog

from package.models.DataFrameModel import FilePreviewModel


class FileDialog(QDialog):

    def __init__(self, parent):
        super().__init__(parent)
        self.shiftumi = parent.shiftumi
        uic.loadUi("designer/filedialog.ui", self)

        # Pandas dataframe model to load tsv files
        self.fileModel = FilePreviewModel(self)

        self.filePreview.setModel(self.fileModel)
        self.filePreview.horizontalHeader().setSectionResizeMode(
            QHeaderView.Stretch)

        # Events  -------------------------------------------------------------
        self.parseOptWidget.optionsChanged.connect(self.updateOptions)
        # Display file content in table on selection
        self.fileLoader.fileList.currentRowChanged.connect(
            self.preview_file_at)
        # Handle reference (step 0) file changes
        self.fileLoader.referenceFileChanged.connect(self.updateReference)
        # Handle file list content changes
        self.fileLoader.fileListChanged.connect(self.filePreview.setEnabled)
        self.fileLoader.fileListChanged.connect(self.toggleValidateBtn)

    @property
    def parseOptions(self):
        return self.parseOptWidget.options

    @pyqtSlot(int)
    def preview_file_at(self, row=-1):
        if row < 0:
            self.fileModel.clear()
            self.fileInfo.display(None)
        else:
            item = self.fileLoader.fileList.item(row)
            df = self.shiftumi.load_csv(item.data(Qt.UserRole)["path"])
            self.fileModel.set_df(df)
            self.fileInfo.display(item)

    @pyqtSlot(str)
    def updateReference(self, referenceFilePath):
        if self.shiftumi.parseOpt["infer"] and referenceFilePath:
            options = self.infer_options(referenceFilePath)
            self.set_options(options)

    @pyqtSlot(dict)
    def updateOptions(self, options):
        if options["infer"]:
            options = self.infer_options(self.fileLoader.referenceFile)
        self.set_options(options)

    def infer_options(self, referenceFilePath: str):
        infered_options = self.shiftumi.infer_parse_options(
            referenceFilePath)
        self.parseOptWidget.set_options(infered_options)
        return infered_options

    def set_options(self, options):
        # Update internal options and reload files using them
        if options != self.shiftumi.parseOpt:
            self.shiftumi.set_parse_options(options)
            self.fileLoader.reload_files()
        # Update UI if necessary
        if options != self.parseOptions:
            self.parseOptWidget.set_options(options)
        return self.shiftumi.parseOpt

    @pyqtSlot()
    def toggleValidateBtn(self):
        okBtn = self.buttonBox.button(QDialogButtonBox.Ok)
        okBtn.setEnabled(self.fileLoader.errors == 0)
