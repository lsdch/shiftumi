from os.path import basename, dirname, abspath

import qtawesome as qta
from PyQt5.QtCore import QRegExp, Qt, pyqtSlot, QUrl
from PyQt5.QtGui import QFont, QDesktopServices
from PyQt5.QtWidgets import QGroupBox, QLabel
from PyQt5 import uic

from package.widgets.FileListWidget import FileListWidget


class FileInfoWidget(QGroupBox):

    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi("designer/fileinfo.ui", self)

        self.data = None

        self.openDirBtn.setIcon(qta.icon('fa.folder'))
        self.openDirBtn.clicked.connect(self.openDirectory)

    @pyqtSlot()
    def openDirectory(self):
        url = QUrl.fromLocalFile(dirname(abspath(self.data["path"])))
        QDesktopServices.openUrl(url)

    def clearLayout(self, layout):
        for i in reversed(range(layout.count())):
            layout.itemAt(i).widget().setParent(None)

    def display(self, item):
        self.clearLayout(self.warningsDisplay)
        self.clearLayout(self.errorsDisplay)

        if item is None:
            self.data = None
            for field in self.findChildren(QLabel, QRegExp(".*Display")):
                field.setText("-")
            self.setDisabled(True)
            self.openDirBtn.setDisabled(True)
        else:
            self.data = item.data(Qt.UserRole)
            # Display file name
            self.fileNameDisplay.setText(basename(self.data["path"]))
            # Display incomplete residues count
            self.incompletesDisplay.setText(str(self.data["incompletes"]))
            if self.data["incompletes"] > 0:
                self.incompletesDisplay.setStyleSheet('color:"orange"')
            else:
                self.incompletesDisplay.setStyleSheet('color:"#99CA53"')
            # Display total residue count
            self.residuesDisplay.setText(str(self.data["residues"]))

            for warning in self.data["warnings"]:
                self.add_warning(warning)

            for error in self.data["errors"]:
                self.add_error(error)
            self.setDisabled(False)
            self.openDirBtn.setDisabled(False)

    def add_warning(self, warning):
        label = QLabel(u"\u2022" + "  " + warning)
        label.setStyleSheet("color:'orange'")
        self.warningsDisplay.addWidget(label)

    def add_error(self, error):
        label = QLabel(u"\u2022" + "  " + error)
        label.setStyleSheet("color:'red'")
        self.errorsDisplay.addWidget(label)
