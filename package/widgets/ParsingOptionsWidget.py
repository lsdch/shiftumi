from PyQt5 import uic
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QGroupBox

from package.widgets.LinkedComboWidget import LinkedComboWidget


class ParsingOptionsWidget(QGroupBox):

    optionsChanged = pyqtSignal(dict)
    inferChanged = pyqtSignal(bool)

    def __init__(self, parent):
        super().__init__(parent)
        uic.loadUi("designer/parseoptions.ui", self)

        self.inferCheckbox.stateChanged.connect(self.infer_changed)
        self.headerCheckbox.stateChanged.connect(self.internalChange)
        self.comboWidget.connectEvents()

        self.inferCheckbox.stateChanged.emit(self.inferCheckbox.isChecked())

    def infer_changed(self, checked):
        self.headerCheckbox.setDisabled(checked)
        self.comboWidget.setDisabled(checked)
        self.optionsChanged.emit(self.options)
        self.inferChanged.emit(checked)

    @pyqtSlot()
    def internalChange(self):
        self.optionsChanged.emit(self.options)

    @property
    def options(self):
        return {
            "infer": self.inferCheckbox.isChecked(),
            "header": self.headerCheckbox.isChecked(),
            "columnMap": self.comboWidget.value()
        }

    def set_options(self, options):
        if options["header"] == "infer":
            header = Qt.Checked
        else:
            header = Qt.Unchecked
        self.headerCheckbox.setCheckState(header)

        self.comboWidget.set_values(options["columnMap"])
