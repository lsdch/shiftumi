from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QGroupBox, QFrame
from PyQt5 import uic

class InitialVolumeWidget(QGroupBox):

    updated = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi("designer/initialvolumewidget.ui", self)

        self.analyteInput.valueChanged.connect(self.totalInput.setMinimum)
        self.analyteInput.valueChanged.connect(self.update_analyte)
        self.totalInput.valueChanged.connect(self.update_total)

    def set_protocole(self, model):
        self.protocole = model
        self.fetch()

    def fetch(self):
        self.analyteInput.setValue(self.protocole.analyteStartVol)
        self.totalInput.setValue(self.protocole.startVol)

    @pyqtSlot("double")
    def update_analyte(self, volume):
        self.protocole.set_analyte_volume(volume)
        self.updated.emit()

    @pyqtSlot("double")
    def update_total(self, volume):
        self.protocole.set_initial_volume(volume)
        self.updated.emit()



class StockSolutionWidget(QFrame):

    updated = pyqtSignal()
    kind = "Solution"

    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi("designer/stocksolution.ui", self)
        self.nameLabel.setText(self.kind)
        self.nameInput.setPlaceholderText(self.kind)
        self.nameInput.textEdited.connect(self.update_name)
        self.concentration.valueChanged.connect(self.update_concentration)

    def set_model(self, solution):
        self.model = solution
        self.fetch()

    def fetch(self):
        self.concentration.setValue(self.model.concentration)
        self.nameInput.setText(self.model.name)

    @pyqtSlot("QString")
    def update_name(self, name):
        self.model.setName(name)
        self.updated.emit()

    @pyqtSlot("int")
    def update_concentration(self, concentration):
        self.model.setConcentration(concentration)
        self.updated.emit()

class AnalyteWidget(StockSolutionWidget):
    kind = "Analyte"

    def __init__(self, parent):
        super().__init__(parent)
        

class TitrantWidget(StockSolutionWidget):
    kind = "Titrant"
    def __init__(self, parent):
        super().__init__(parent)
