import json
import os
import sys
from collections import OrderedDict

import pandas as pd
import yaml


# Setup YAML for ordered dict output : https://stackoverflow.com/a/8661021
def represent_dict_order(self, data):
    return self.represent_mapping('tag:yaml.org,2002:map', data.items())


yaml.add_representer(OrderedDict, represent_dict_order)

# Setup pandas float precision
pd.set_option('precision', 3)


# --------------------------------------------------------------------
# Protocole classes
# --------------------------------------------------------------------

class StockSolution(object):

    def __init__(self, kind, name=None, concentration=1):
        self.kind = kind
        self.setName(name)
        self.setConcentration(concentration)

    def __repr__(self):
        return "{} [{}]".format(self.name, self.concentration)

    def setConcentration(self, value):
        if value >= 0:
            self.concentration = value
            return True
        else:
            return False

    def setName(self, name):
        if name:
            name = str(name)
        self._name = name

    @property
    def name(self):
        return self._name or self.kind

    @classmethod
    def from_dict(cls, d):
        return cls(**d)


class TitrationProtocole(object):
    "A titration protocole tracker, wrapping around a pandas dataframe"

    INIT_FIELDS = ('name', 'analyte', 'titrant', 'start_volume', 'add_volumes')
    COLUMN_ALIASES = ('vol_add', 'vol_titrant', 'vol_total', 'conc_titrant',
                      'conc_analyte', 'ratio', 'step', 'file')

    def __init__(self, **kwargs):

        # Initial titrant concentration
        self.titrant = StockSolution("Titrant", concentration=1)

        # Initial analyte concentration
        self.analyte = StockSolution("Analyte", concentration=1)

        # Initial total volume
        self.startVol = 1
        # Initial analyte volume in total volume
        self.analyteStartVol = 1

        # Added titrant volumes : 0 for first step
        self.volumes = [0]

        # Initialize protocole dataframe
        self._df = pd.DataFrame(
            index=list(range(len(self.volumes))) or [0],
            columns=self.COLUMN_ALIASES,
            data=0)
        self.col_aliases = dict(zip(self.COLUMN_ALIASES, self._df.columns))

    # ----------------------------------------------------------
    # Protocole using pandas
    # ----------------------------------------------------------

    def __getitem__(self, item):
        "Get item from data frame using column alias"
        if item in self.col_aliases:
            return self._df[self.col_aliases[item]]
        else:
            return self._df[item]

    def __setitem__(self, item, value):
        "Set item from data frame using column alias"
        if item in self.col_aliases:
            self._df[self.col_aliases[item]] = value
        else:
            self._df[item] = value

    @property
    def df(self):
        "Property for getting inner dataframe"
        return self._df

    def update(self, index=True):
        "Rebuild dataframe from current volumes list"
        self.fill_df()
        # self.set_headers()
        return self._df

    def fill_df(self):
        "Fill dataframe columns"
        self._df['step'] = list(range(len(self.volumes)))
        self._df['vol_add'] = self.volumes
        self._df['vol_titrant'] = self._df['vol_add'].cumsum()
        self._df['vol_total'] = self.startVol + self._df['vol_titrant']
        self._df['conc_titrant'] = self._df['vol_titrant'] * \
            self.titrant.concentration / self._df['vol_total']
        self._df['conc_analyte'] = self.analyteStartVol * \
            self.analyte.concentration / self._df['vol_total']
        self._df['ratio'] = self._df['conc_titrant'] / self._df['conc_analyte']

    def set_file_list(self, files: list):
        self._df['file'] = pd.Series(files)

    def set_headers(self):
        """Set more expressive column headers for display
        and register headers aliases for easy access to data
        """
        # Create headers list
        headers = list(map(
            lambda s: s.format(
                titrant=self.titrant.name,
                analyte=self.analyte.name),
            [
                'Added {titrant} (µL)',
                'Total {titrant} (µL)',
                'Total volume (µL)',
                '[{titrant}] (µM)',
                '[{analyte}] (µM)',
                '[{titrant}]/[{analyte}]'
            ]))
        # update aliases
        self.col_aliases = dict(zip(self.COLUMN_ALIASES, headers))
        # update headers
        self._df.columns = list(headers)

# -----------------------------------------------------
# Input/output
# -----------------------------------------------------

    def validate(self):
        valid = True

        if not self.titrant.name:
            self.titrant.name = 'titrant'

        if not self.analyte.name:
            self.analyte.name = 'analyte'

        if self.titrant.concentration <= 0:
            self.titrant.concentration = 0
            valid = False

        if self.analyte.concentration <= 0:
            self.analyte.concentration = 0
            valid = False

        if self.startVol <= 0:
            self.startVol = 0
            valid = False
        if self.analyteStartVol <= 0:
            self.analyteStartVol = 0
            valid = False
        if self.analyteStartVol >= self.startVol:
            valid = False

        if self.volumes[0] != 0:
            valid = False

        return valid

# -------------------------------------------------
# Manipulation methods
# -------------------------------------------------

    def set_volumes(self, volumes: list):
        "Set tiration volumes, updating steps to match number of volumes"
        if volumes[0] != 0:
            volumes.insert(0, 0)
        self.volumes = list(map(float, volumes))
        self.update()

    def update_volumes(self, stepVolumes: dict):
        "Updates protocole volumes from a dict \{step_nb: volume\}"
        try:
            for step, vol in stepVolumes.items():
                self.volumes[step] = vol
        except IndexError:
            print("{step} does not exist".format(step=step), file=sys.stderr)

    def add_volume(self, volume: float):
        "Add a volume for next protocole step"
        self.volumes.append(volume)
        self._df = self.df.append(dict.fromkeys(self.COLUMN_ALIASES, 0),
                                  ignore_index=True)
        self.update()

    def add_volumes(self, volumes):
        "Add a list of volumes for next protocole steps"
        self.volumes += volumes
        self.update()

    def remove_step(self, step):
        self.remove_steps([step])

    def remove_steps(self, steps):
        self._df = self.df.drop(steps)
        self._df = self._df.reset_index(drop=True)
        for step in steps:
            self.volumes.pop(step)
        self.update()

    def set_analyte_volume(self, volume: float):
        if volume <= 0:
            raise ValueError("Analyte volume must be strictly positive")
        else:
            self.analyteStartVol = volume

    def set_initial_volume(self, volume: float):
        if volume < self.analyteStartVol:
            raise ValueError(
                "Total volume must be greater or equal to analyte volume")
        else:
            self.startVol = volume
