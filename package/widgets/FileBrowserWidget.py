import qtawesome as qta
from PyQt5 import uic
from PyQt5.QtCore import QStandardPaths, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QFileDialog, QGroupBox, QProgressBar

from package.widgets.FileListWidget import FileListWidget


class FileBrowserWidget(QGroupBox):

    addFiles = pyqtSignal(list)
    addDir = pyqtSignal(str)

    def __init__(self, parent):
        super().__init__(parent)

        # Setup ui
        uic.loadUi("designer/filebrowser.ui", self)
        self.browseFileBtn.setIcon(qta.icon('fa.files-o', color="orange"))
        self.browseDirBtn.setIcon(qta.icon('fa.folder', color="lightgreen"))

        # Setup browse events
        self.browseFileBtn.clicked.connect(self.browseFiles)
        self.browseDirBtn.clicked.connect(self.browseDir)

    @pyqtSlot()
    def browseDir(self):
        """Allows selecting a directory to load RMN data files
        """
        # Open file dialog
        directory = QFileDialog.getExistingDirectory(
            self.window(),
            "Open RMN data directory",
            QStandardPaths.writableLocation(QStandardPaths.DocumentsLocation))
        if directory:  # directory selected
            self.addDir.emit(directory)

        return directory

    @pyqtSlot()
    def browseFiles(self):
        """Allows selecting a directory to load RMN data files
        """
        # Open file dialog
        files = QFileDialog.getOpenFileNames(
            self.window(),
            "Open RMN data files",
            QStandardPaths.writableLocation(QStandardPaths.DocumentsLocation),
            "TSV files (*.csv *.tsv *.list)")
        print(files)
        pathList = sorted(files[0])
        print(pathList)
        if pathList:
            self.addFiles.emit(pathList)
        return pathList

    def setMaxProgress(self, maximum):
        self.progress.setRange(0, max(2, maximum))

    @pyqtSlot()
    @pyqtSlot("QListWidgetItem")
    def progressNext(self):
        self.progress.setValue(self.progress.value() + 1)

    def setProgress(self, value):
        self.progress.setValue(value)

    def fileRemoved(self):
        self.progress.setRange(0, max(2, self.progress.maximum() - 1))
