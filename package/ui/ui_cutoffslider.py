# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'designer/cutoffslider.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_cutoffWidget(object):
    def setupUi(self, cutoffWidget):
        cutoffWidget.setObjectName("cutoffWidget")
        cutoffWidget.resize(60, 300)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(cutoffWidget.sizePolicy().hasHeightForWidth())
        cutoffWidget.setSizePolicy(sizePolicy)
        cutoffWidget.setMaximumSize(QtCore.QSize(97, 16777215))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        cutoffWidget.setFont(font)
        cutoffWidget.setStyleSheet("QGroupBox{\n"
"border:none;\n"
"margin-top: 1.5em;\n"
"}")
        cutoffWidget.setAlignment(QtCore.Qt.AlignCenter)
        self.verticalLayout = QtWidgets.QVBoxLayout(cutoffWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.cutoffSlider = QtWidgets.QSlider(cutoffWidget)
        self.cutoffSlider.setCursor(QtGui.QCursor(QtCore.Qt.OpenHandCursor))
        self.cutoffSlider.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.cutoffSlider.setSingleStep(1)
        self.cutoffSlider.setPageStep(10)
        self.cutoffSlider.setProperty("value", 0)
        self.cutoffSlider.setSliderPosition(0)
        self.cutoffSlider.setTracking(False)
        self.cutoffSlider.setOrientation(QtCore.Qt.Vertical)
        self.cutoffSlider.setInvertedAppearance(False)
        self.cutoffSlider.setObjectName("cutoffSlider")
        self.verticalLayout.addWidget(self.cutoffSlider, 0, QtCore.Qt.AlignHCenter)
        self.cutoffSpinBox = QtWidgets.QDoubleSpinBox(cutoffWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cutoffSpinBox.sizePolicy().hasHeightForWidth())
        self.cutoffSpinBox.setSizePolicy(sizePolicy)
        self.cutoffSpinBox.setMaximumSize(QtCore.QSize(60, 16777215))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(False)
        font.setWeight(50)
        self.cutoffSpinBox.setFont(font)
        self.cutoffSpinBox.setStyleSheet("")
        self.cutoffSpinBox.setWrapping(False)
        self.cutoffSpinBox.setFrame(True)
        self.cutoffSpinBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.cutoffSpinBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.PlusMinus)
        self.cutoffSpinBox.setAccelerated(True)
        self.cutoffSpinBox.setCorrectionMode(QtWidgets.QAbstractSpinBox.CorrectToPreviousValue)
        self.cutoffSpinBox.setKeyboardTracking(True)
        self.cutoffSpinBox.setProperty("showGroupSeparator", False)
        self.cutoffSpinBox.setDecimals(3)
        self.cutoffSpinBox.setMaximum(100.0)
        self.cutoffSpinBox.setSingleStep(0.05)
        self.cutoffSpinBox.setObjectName("cutoffSpinBox")
        self.verticalLayout.addWidget(self.cutoffSpinBox, 0, QtCore.Qt.AlignHCenter)

        self.retranslateUi(cutoffWidget)
        QtCore.QMetaObject.connectSlotsByName(cutoffWidget)

    def retranslateUi(self, cutoffWidget):
        _translate = QtCore.QCoreApplication.translate
        cutoffWidget.setWindowTitle(_translate("cutoffWidget", "GroupBox"))
        cutoffWidget.setTitle(_translate("cutoffWidget", "Cut-off"))
        self.cutoffSlider.setToolTip(_translate("cutoffWidget", "<html><head/><body><p>Set cut-off</p></body></html>"))
        self.cutoffSlider.setStatusTip(_translate("cutoffWidget", "Cut-off"))
