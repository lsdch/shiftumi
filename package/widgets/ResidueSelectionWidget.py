import qtawesome as qta
from PyQt5.QtWidgets import QListView, QWidget

from package.models.DataFrame3DModel import SelectedResiduesModel

# from package.views.ResidueListView import ResidueListView



class ResidueSelectionWidget(QWidget):

    def __init__(self, parent):
        super().__init__(parent)

    def setModel(self, model: SelectedResiduesModel):
        self.residueList = self.findChild(QListView)
        self.setBtn = self.window().selSetBtn
        self.addBtn = self.window().selAddBtn
        self.removeBtn = self.window().selRemoveBtn
        self.clearBtn = self.window().selClearBtn

        self.residueList.setModel(model)

        self.setBtn.clicked.connect(model.setSelection)
        self.addBtn.clicked.connect(model.addSelection)
        self.clearBtn.clicked.connect(model.init_selection)
