import sys
from multiprocessing import Process
from threading import Thread

# QT Components
import qtawesome as qta
from PyQt5 import uic
from PyQt5.QtCore import Qt, QThreadPool, pyqtSlot
from PyQt5.QtWidgets import QApplication, QMainWindow, qApp

# Shiftumi App Components
from package.classes.Shiftumi import Shiftumi
from package.classes.Worker import Worker
# Controllers
from package.controllers.BarChartController import BarChartController
from package.controllers.FileListController import FileListController
from package.controllers.ProtocoleController import ProtocoleController
# Dialogs
from package.dialogs.FileDialog import FileDialog
# Models
from package.models.DataFrame3DModel import (DataFrame3DModel,
                                             IntensityDataFrameModel,
                                             SelectedResiduesModel)


class MainWindow(QMainWindow):
    """Shift2Me GUI main window class
    """

    def __init__(self, shiftumi, parent=None):

        # Init QWidget main window
        super().__init__()

        # Setup base UI template
        uic.loadUi("designer/mainwindow.ui", self)
        self.patch_ui()

        # Setup window properties
        self.setObjectName("MainWindow")
        self.resize(900, 600)

        # Shiftumi main component
        self.shiftumi = shiftumi

        # Init file parsing options
        self.parseOptions = dict()

        # MODELS
        # Model interface for main shiftumi dataframe
        self.model = DataFrame3DModel(self.shiftumi, self)
        # Chemical shift intensities model
        self.intensityModel = IntensityDataFrameModel(self.shiftumi, self)
        # Selected residues model
        self.selectionModel = SelectedResiduesModel(self)
        self.selectionModel.setSourceModel(self.intensityModel)

        # CONTROLLERS
        self.init_controllers()

        # Setup file selection dialog
        self.fileDialog = FileDialog(self)
        self.editFilesBtn.clicked.connect(self.manage_files)
        self.fileDialog.accepted.connect(self.updateModels)

        # Set up parallel workers to compute intensities
        self.threadPool = QThreadPool(self)
        self.threadPool.setMaxThreadCount(1)
        self.intensityWorker = Worker(self.update_intensities)
        self.intensityWorker.setAutoDelete(False)

        # Update tabs enabled/disabled state
        self.toggleTabs()

    def init_controllers(self):
        """
        Initialize app component controllers.
        Called once in MainWindow.__init__()
        """
        # File List Controller
        self.fileListCtrl = FileListController(self)
        # BarChart
        self.barChartController = self.barChartController
        self.barChartController.init_model(self.intensityModel,
                                           self.selectionModel)
        # Protocole
        self.protocoleCtrl = self.protocoleTab   # ProtocoleController(self)
        self.protocoleCtrl.set_protocole(self.shiftumi.protocole)
        # Shiftmap
        self.shiftmapCtrl = self.shiftmapController
        self.shiftmapCtrl.setModel(self.model, self.selectionModel)

    @pyqtSlot()
    def updateModels(self):
        "Update Shiftumi models when input file list is changed"

        QApplication.setOverrideCursor(Qt.WaitCursor)

        self.shiftumi.set_parse_options(self.fileDialog.parseOptions)

        self.model.layoutAboutToBeChanged.emit()

        # Reset Shiftumi dataframes
        self.shiftumi.init_dataframes()

        # Load input file data
        fileList = self.fileDialog.fileLoader.fileList

        for row in range(len(fileList)):
            item = fileList.item(row)
            df = self.shiftumi.load_csv(
                filePath=item.data(Qt.UserRole)["path"])
            self.shiftumi.set_step_dataframe(df, row)

        self.shiftumi.update_incomplete_residues()

        # Finalize model changes
        self.model.layoutChanged.emit()

        # Update file list content to be displayed in main window
        self.fileListCtrl.loadList(fileList)

        # self.threadPool.start(self.intensityWorker)

        # Update intensities, ptotocole and shiftmap in main window
        self.update_intensities()
        self.shiftmapCtrl.update()
        self.update_protocole()

        QApplication.restoreOverrideCursor()

    def update_intensities(self):
        "Update intensity barchart and reset residue selection to be empty"

        self.intensityModel.update()
        self.toggleTabs()
        self.barChartController.update()
        self.selectionModel.init_selection()

    @pyqtSlot()
    def update_protocole(self, fileNames=[]):
        "Update protocole with new file names"

        fileNames = fileNames or self.fileListCtrl.fileList.itemNames
        self.protocoleCtrl.update_file_list(fileNames)

    def toggleTabs(self):
        """
        Toggle tabs based on the number of titration step,
        i.e number of input files
        """
        stepCount = self.model.subRowCount()
        self.tabs.setTabEnabled(2, stepCount > 1)
        self.tabs.setTabEnabled(3, stepCount > 0)

    def edit_stock(self, event):
        # UNUSED
        pass
        # stockDialog = StockDialog()
        # stockDialog.exec()

    def manage_files(self, event):
        "Open input file selection dialog"

        self.fileDialog.exec()

    def patch_ui(self):
        self.tabs.setTabIcon(0, qta.icon('fa.table'))
        self.tabs.setTabIcon(1, qta.icon('fa.cogs'))
        self.tabs.setTabIcon(2, qta.icon('fa.bar-chart'))
        self.tabs.setTabIcon(3, qta.icon('fa.bullseye'))


def run():
    "Start application main loop"

    # Create QT app
    app = QApplication(sys.argv)
    app.setApplicationName("Shiftumi")

    shiftumi = Shiftumi()
    # Build and display main window
    window = MainWindow(shiftumi)
    window.show()
    return app.exec()
