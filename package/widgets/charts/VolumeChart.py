from math import ceil

from PyQt5.QtChart import QChart, QLineSeries, QValueAxis, QVXYModelMapper
from PyQt5.QtCore import QMargins, Qt, pyqtSlot
from PyQt5.QtGui import QPainter


class VolumeChart(QChart):

    def __init__(self, model, chartView):
        super().__init__()

        # Create series
        self.init_series()
        # Init model mappers
        self.init_mappers(model)
        # Init chart axis and attach series
        self.init_axis(model)
        # Attach to chart to view
        chartView.setChart(self)
        chartView.setRenderHint(QPainter.Antialiasing)
        self.layout().setContentsMargins(2, 2, 2, 2)
        self.setMargins(QMargins(5, 5, 5, 5))

    def init_series(self):
        self.volumeSeries = QLineSeries()
        self.volumeSeries.setName("Titrant")
        self.volumeSeries.setPointsVisible()

        self.totalVolumeSeries = QLineSeries()
        self.totalVolumeSeries.setName("Total")
        self.totalVolumeSeries.setPointsVisible()

        self.addSeries(self.volumeSeries)
        self.addSeries(self.totalVolumeSeries)

    def init_mappers(self, model):
        # Titrant volume mapper
        self.mapper = QVXYModelMapper(self)
        self.mapper.setXColumn(6)
        self.mapper.setYColumn(1)
        self.mapper.setSeries(self.volumeSeries)
        # Set model
        self.mapper.setModel(model)

        # Total volume mapper
        self.totalVolumeMapper = QVXYModelMapper(self)
        self.totalVolumeMapper.setXColumn(6)
        self.totalVolumeMapper.setYColumn(2)
        self.totalVolumeMapper.setSeries(self.totalVolumeSeries)
        # Set model
        self.totalVolumeMapper.setModel(model)

    def init_axis(self, model):
        # Step Axis
        self.stepAxis = QValueAxis()
        self.stepAxis.setTickCount(model.rowCount() + 1)
        self.stepAxis.setTitleText("Step")
        self.stepAxis.setLabelFormat("%i")
        self.setAxisX(self.stepAxis, self.volumeSeries)
        # Volumes Axis
        self.volumeAxis = QValueAxis()
        # self.volumeAxis.setLinePenColor(self.volumeSeries.pen().color())
        self.volumeAxis.setTitleText("Volume (µL)")
        self.addAxis(self.volumeAxis, Qt.AlignLeft)
        # Attach axis to series
        self.volumeSeries.attachAxis(self.volumeAxis)
        self.totalVolumeSeries.attachAxis(self.stepAxis)
        self.totalVolumeSeries.attachAxis(self.volumeAxis)

    @pyqtSlot(int, float)
    def update_axis(self, rowCount, maxVol):
        self.axisX().setRange(0, rowCount - 1)
        self.axisX().setTickCount(rowCount)
        maxRange = ceil(maxVol / 2.0) * 2
        self.axisY().setRange(0, maxRange)
        self.axisY().setTickCount(6)
        self.axisY().applyNiceNumbers()
        # ticks = maxRange/2 + 1
        # if ticks < 7:
        #     self.axisY().setTickCount(ticks)
        self.mapper.setXColumn(6)
        self.totalVolumeMapper.setXColumn(6)
