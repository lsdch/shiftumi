import numpy as np
import pandas as pd
from PyQt5.QtCore import (QAbstractTableModel, QModelIndex, Qt, QVariant,
                          pyqtSignal, pyqtSlot)
from PyQt5.QtGui import QColor


class DataFrameModel(QAbstractTableModel):

    removeDisabled = pyqtSignal(bool)

    def __init__(self, parent=None, editable=False, df=None):
        super().__init__(parent)
        if df is None:
            self._df = pd.DataFrame()
        else:
            self._df = df
        self.editable = editable

    @property
    def df(self):
        return self._df

    def load_csv(self, filePath: str, header, index=None):
        """Loads a CSV-like file as a pandas dataframe

        Arguments:
            filePath {string} -- path of file
            header {bool} -- parse headers
            index {list} -- column number to use as index
        """

        with open(filePath, "r") as fh:
            self.layoutAboutToBeChanged.emit()
            self._df = pd.read_table(fh,
                                     sep='\\s+',
                                     header=header,
                                     float_precision="high")
            if index is not None:
                self._df.set_index(self._df.columns[index], inplace=True)
            self.layoutChanged.emit()

    def clear(self):
        self.layoutAboutToBeChanged.emit()
        self._df = pd.DataFrame()
        self.layoutChanged.emit()

    def rowCount(self, parent=QModelIndex()):
        return len(self.df.index)

    def columnCount(self, parent=QModelIndex()):
        return len(self.df.columns.values)

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return QVariant()

        if orientation == Qt.Horizontal:
            try:
                return str(self.df.columns.tolist()[section])
            except (IndexError, ):
                return QVariant()
        elif orientation == Qt.Vertical:
            try:
                # return self.df.index.tolist()
                return str(self.df.index.tolist()[section])
            except (IndexError, ):
                return QVariant()

    def data(self, index, role=Qt.DisplayRole):
        """Returns data from model at given index
        """

        row = self.df.index[index.row()]
        col = self.df.columns[index.column()]

        if not index.isValid():
            return None
        if role == Qt.DisplayRole or role == Qt.UserRole:
            return QVariant(str(self.df.loc[row, col]))
        return None

    def sort(self, column, order):
        colname = self.df.columns.tolist()[column]
        self.layoutAboutToBeChanged.emit()
        self.df.sort_values(colname, ascending=order ==
                            Qt.AscendingOrder, inplace=True)
        self.df.reset_index(inplace=True, drop=True)
        self.layoutChanged.emit()

    def flags(self, index):
        # return super().flags(index)
        flags = super().flags(index)
        if self.editable:
            flags |= Qt.ItemIsEditable
        return flags

    def setData(self, index, value, role=Qt.DisplayRole):
        """Set the value to the index position depending on
        Qt::ItemDataRole and data type of the column

        Args:
            index (QModelIndex): Index to define column and row.
            value (object): new value.
            role (Qt::ItemDataRole): Specifies what you want to do.
        Raises:
            TypeError: If the value could not be converted to a known datatype.
        Returns:
            True if value is changed. Calls layoutChanged after update.
            False if value is not different from original value.
        """
        if not index.isValid():
            return False

        if value != index.data(role):

            self.layoutAboutToBeChanged.emit()
            row = self.df.index[index.row()]
            col = self.df.columns[index.column()]
            self.df.set_value(row, col, value)
            self.layoutChanged.emit()
            return True
        else:
            return False


class FilePreviewModel(DataFrameModel):

    columnNames = ["Proton", "Nitrogen"]

    def __init__(self, parent=None):
        super().__init__(parent, editable=False, df=None)

        # 0: residues
        # 1: proton
        # 2: nitrogen
        self.parseOpt = {
            "header": "infer",
            "columnMap": [0, 1, 2]
        }
        self.path = None

    def set_df(self, df):
        self.layoutAboutToBeChanged.emit()
        self._df = df
        self.layoutChanged.emit()

    def data(self, index, role=Qt.DisplayRole):
        """Returns data from model at given index
        """

        row = self.df.index[index.row()]
        col = self.df.columns[index.column()]

        if not index.isValid():
            return None
        if role in (Qt.DisplayRole, Qt.UserRole):
            return QVariant(str(self.df.loc[row, col]))
        elif role == Qt.BackgroundRole:
            val = self.df.loc[row, col]
            if not isinstance(val, np.float64) or val == 0:
                return QColor(255, 0, 0, 50)
        return None

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role != Qt.DisplayRole:
            return QVariant()

        if orientation == Qt.Horizontal:
            try:
                return "{prefix} : {header}".format(
                    prefix=self.columnNames[section],
                    header=str(self.df.columns[section]))
            except (IndexError, ):
                return QVariant()
        elif orientation == Qt.Vertical:
            try:
                return str(self.df.index[section])
            except (IndexError, ):
                return QVariant()

    def load_csv(self, filePath: str):
        """Loads a CSV-like file as a pandas dataframe

        Arguments:
            filePath {string} -- path of file
        """

        with open(filePath, "r") as fh:
            self.layoutAboutToBeChanged.emit()
            self._df = pd.read_table(fh,
                                     sep='\\s+',
                                     header=self.parseOpt["header"],
                                     float_precision="high")
            self._df.fillna(0, inplace=True)
            self._df = self._df.reindex(
                columns=self._df.columns[self.parseOpt["columnMap"]])
            self.df.set_index(self._df.columns[0], inplace=True)
            self.layoutChanged.emit()
        self.path = filePath

    def completeCount(self):
        return np.sum(self.df.all(axis="columns"))

    @pyqtSlot(dict)
    def set_options(self, options):
        options["header"] = "infer" if options["header"] else None
        self.parseOpt = options
        if self.path:
            self.load_csv(self.path)
