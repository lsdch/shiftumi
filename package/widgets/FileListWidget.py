import re

import qtawesome as qta
from PyQt5 import QtGui
from PyQt5.QtCore import QFileInfo, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QFileIconProvider, QListWidget, QListWidgetItem

from package.delegates.FileItemDelegate import (FileItemDelegate,
                                                SelectedFileItemDelegate)


class BaseFileListWidget(QListWidget):

    # File icon provider
    iconProvider = QFileIconProvider()

    def __init__(self, parent):
        super().__init__(parent)
        self.defaultDelegate = FileItemDelegate(self)
        self.delegate = SelectedFileItemDelegate(self)
        self.setItemDelegate(self.defaultDelegate)
        self.previousRow = None
        self.currentRowChanged.connect(self.switchItemDelegate)

    @pyqtSlot(int)
    def switchItemDelegate(self, row):
        if self.previousRow is not None:
            self.setItemDelegateForRow(self.previousRow, self.defaultDelegate)
        self.setItemDelegateForRow(row, self.delegate)
        self.previousRow = row

    @property
    def itemNames(self):
        return [self.item(i).text() for i in range(len(self))]

    def selectDefaultRow(self):
        """Select first row if none is selected
        """
        if self.currentRow() < 0 and len(self):
            self.setCurrentRow(0)
        else:
            self.currentRowChanged.emit(self.currentRow())

    def mouseReleaseEvent(self, event):
        """
        Ignore mouse events not targeting an item
        to prevent losing selection
        """
        if self.itemAt(event.pos()):
            super().mouseReleaseEvent(event)

    def clone(self, sourceList):
        self.clear()
        for row in range(len(sourceList)):
            item = sourceList.item(row)
            self.insertItem(row, QListWidgetItem(item))
        self.selectDefaultRow()


class FileListWidget(BaseFileListWidget):

    # Regex for external file drop
    acceptedExtensions = re.compile(r"(.*\.list|.*\.tsv|.*\.csv)")

    # Emit on list content change
    fileAdded = pyqtSignal("QListWidgetItem")
    fileRemoved = pyqtSignal(int)

    # Emitted upon external files/directory drop
    filesDropped = pyqtSignal(list)
    directoryDropped = pyqtSignal(str)

    # Emitted on list reorder using drag n drop
    listReordered = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        # Keep selection on internal moves
        self.internalMode = False
        self.model().rowsRemoved.connect(self.restoreSelection)
        # Update preview on editor close
        self.itemDelegate().closeEditor.connect(self.editingFinished)

    @pyqtSlot("QString")
    def set_current_item_text(self, text):
        """Edits currently selected item text
        """
        item = self.currentItem()
        if item is not None:
            item.setText(text)

    def editingFinished(self):
        self.currentRowChanged.emit(self.currentRow())

    def add_file(self, info: QFileInfo, diagnostic: dict):
        """Add a single file from its path
        """
        absolutePath = info.absoluteFilePath()

        # Create item
        item = QListWidgetItem(absolutePath)
        # Set item display text
        item.setText(info.baseName())
        # Set item icon
        icon = self.iconProvider.icon(info)
        item.setIcon(QtGui.QIcon(icon))
        # Add full path as item data
        itemData = {
            "path": absolutePath
        }
        itemData.update(diagnostic)
        item.setData(Qt.UserRole, itemData)
        item.setData(Qt.ToolTipRole, "Double-click to edit name")
        item.setFlags(item.flags() | Qt.ItemIsEditable)

        self.addItem(item)
        # Emit signals
        self.fileAdded.emit(item)
        return item

    def remove_selected(self):
        """Removes selected rows from model
        """
        removed = set()
        selectRow = -1
        for item in self.selectedItems():
            row = self.row(item)
            selectRow = row     # Select new row if possible
            if row >= len(self) - 1:
                selectRow -= 1  # Select previous row
            # Remove item
            self.takeItem(row)
            removed.add(item.data(Qt.UserRole)["path"])
            # Emit removal signal
            self.fileRemoved.emit(row)
        # Select target row
        if selectRow == self.currentRow():
            self.currentRowChanged.emit(selectRow)
        self.setCurrentRow(selectRow)
        return removed

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.acceptProposedAction()
        else:
            super().dragEnterEvent(event)

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(Qt.CopyAction)
            event.accept()
        else:
            super().dragMoveEvent(event)

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            droppedFiles = []
            droppedDirs = []
            for url in event.mimeData().urls():
                info = QFileInfo(url.path())
                if self.acceptedExtensions.match(info.absoluteFilePath()):
                    droppedFiles.append(info.absoluteFilePath())
                elif info.isDir():
                    droppedDirs.append(str(url.toLocalFile()))
            if droppedFiles:
                self.filesDropped.emit(droppedFiles)
            for dropDir in droppedDirs:
                self.directoryDropped.emit(dropDir)
            event.acceptProposedAction()
        else:
            if event.source() is self:
                self.internalMode = True
            super().dropEvent(event)

    @pyqtSlot("QModelIndex", int, int)
    def restoreSelection(self, parent, start, end):
        """Restore selection after internal move
        """
        if self.internalMode:
            if start < self.targetRow:
                self.targetRow -= 1
            self.setCurrentRow(self.targetRow)
        self.internalMode = False
        self.listReordered.emit()

    @pyqtSlot("QModelIndex", int, int)
    def rowsInserted(self, parent, start: int, end: int):
        """
        While executing internal moves, save insertion row to restore selection
        """
        super().rowsInserted(parent, start, end)
        if self.internalMode:
            self.targetRow = start
            # Restore dropped item flags
            for row in range(start, end+1):
                item = self.item(row)
                item.setFlags(item.flags() | Qt.ItemIsEditable)
