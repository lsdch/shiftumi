import qtawesome as qta
from PyQt5 import QtWidgets
from PyQt5.QtCore import QStandardPaths, QDir, QFile
from package.ui.ui_stock_dialog import Ui_Dialog


class StockDialog(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.patch_ui()
        self.saveLocation = QStandardPaths.writableLocation(
            QStandardPaths.DataLocation)
        self.saveDir = QDir(self.saveLocation)
        # if (self.saveDir.mkpath(self.saveDir.absolutePath))

    def patch_ui(self):
        self.ui.addBtn.setIcon(qta.icon('fa.plus'))
        self.ui.updateBtn.setIcon(qta.icon('fa.edit'))
        self.ui.deleteBtn.setIcon(qta.icon('fa.remove'))
