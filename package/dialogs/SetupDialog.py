from collections import OrderedDict

import qtawesome as qta
from PyQt5 import QtWidgets
from PyQt5.QtCore import QStandardPaths, Qt
from PyQt5.QtCore import pyqtSlot as Slot

from package.controllers.LinkedComboController import LinkedComboController
from package.delegates.SpinBoxDelegate import SpinBoxDelegate
from package.models.DataFrameModel import DataFrameModel
from package.ui.ui_setupdialog import Ui_Dialog
from package.widgets.DynamicComboWidget import DynamicComboWidget


class SetupDialog(QtWidgets.QDialog):

    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.patch_ui()

        # Display file content in table on selection
        self.ui.fileListWidget.currentItemChanged.connect(self.show_preview)

        # Pandas dataframe model to load tsv files
        self.fileModel = DataFrameModel(self)
        self.ui.filePreview.setModel(self.fileModel)

        # Header choices
        self.atomCombo = OrderedDict([
            ("residue", self.ui.residueColumn),
            ("proton", self.ui.protonColumn),
            ("nitrogen", self.ui.nitrogenColumn)
        ])
        self.comboController = LinkedComboController(self, self.atomCombo)

        # Include CSV headers
        self.ui.headerCheckbox.stateChanged.connect(self.reload_preview)
        self.setwd(QStandardPaths.writableLocation(
            QStandardPaths.DocumentsLocation))

    def show_preview(self, current, previous):
        self.load_csv(current)

    def reload_preview(self):
        selected = self.ui.fileListWidget.selectedIndexes().pop()
        self.load_csv(selected)

    def load_csv(self, item):
        """Loads a CSV file and display it
        """

        # Use header if user wants to
        useHeaders = 'infer' if self.ui.headerCheckbox.isChecked() else None

        # Load file as pandas dataframe
        self.fileModel.load_csv(
            filePath=item.data(Qt.UserRole),
            header=useHeaders)

        # Update combo options
        options = ["Col {}".format(idx)
                   for idx in range(len(self.fileModel.df.columns))]
        if useHeaders:
            headers = self.fileModel.df.columns.tolist()
            options = [(" : ").join(label) for label in zip(options, headers)]
        self.comboController.update_options(options)

    def browse(self, event):
        """Allows selecting a directory to load RMN data files
        """
        # Open file dialog
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            "RMN Data directory",
            QStandardPaths.writableLocation(QStandardPaths.DocumentsLocation))
        if directory:  # directory selected
            self.setwd(directory)
        return directory

    def setwd(self, directory):
        """Sets the directory to traverse for RMN data files
        """
        # Update current working directory label in UI
        self.ui.cwdLabel.setText(directory)
        # Load content
        self.ui.fileListWidget.set_directory(directory)
        self.toggleUI(len(self.ui.fileListWidget))

    def auto_select_first(self, path):
        """Selects first file in directory on load
        """
        if self.dirModel.rowCount(self.parentIndex):
            self.toggleUI(True)
            # self.ui.fileListWidget.setCurrentIndex(
            #     self.dirModel.index(0, 0, self.dirModel.index(path))
            # )
            self.reload_preview()
        else:
            self.toggleUI(False)

    def patch_ui(self):
        self.ui.editProtocoleBtn.setIcon(qta.icon('fa.cogs'))

    def toggleUI(self, toggle=True):
        """Enables and disables UI elements
        """
        self.ui.fileListWidget.setEnabled(toggle)
        for combo in self.atomCombo.values():
            combo.setEnabled(toggle)
        self.ui.filePreview.setEnabled(toggle)
        self.ui.headerCheckbox.setEnabled(toggle)
