from PyQt5.QtWidgets import (QFrame, QPushButton, QCheckBox,
                             QGroupBox, QListView, QApplication)
from PyQt5.QtChart import QChartView
from PyQt5.QtCore import Qt, pyqtSlot, QVariant
from PyQt5.QtGui import QPainter


from package.models.DataFrame3DModel import ShiftmapModel
from package.widgets.charts.ShiftmapChart import ShiftmapChart

import qtawesome as qta
from collections import OrderedDict


class ShiftmapController(QFrame):

    def __init__(self, parent):
        super().__init__(parent)

        self.chart = ShiftmapChart()
        self.selectedList = None

        self.seriesMap = OrderedDict()
        self.mapperMap = OrderedDict()

    def setModel(self, model, selectionModel):

        self.selectionModel = selectionModel
        self.selectedList = self.findChild(QListView)
        self.selectedList.setModel(selectionModel)
        selectionModel.selectionChanged.connect(self.selectionChanged)
        self.selectedList.currentRowChanged.connect(self.selectResidue)

        self.model = ShiftmapModel(self)
        self.model.setSourceModel(model)
        self.chart.setModel(self.model)

        self.controls = self.findChild(QGroupBox, "shiftmapCtrlBox")

        # Control toggling shiftmap filtered display
        self.toggleBtn = self.findChild(QPushButton, "shiftmapFilterBtn")
        self.toggleBtn.toggled.connect(self.toggle_filter)
        self.toggleBtn.setIcon(qta.icon("fa.filter"))

        # Control filtered shiftmap scaling
        self.scaleCheckbox = self.findChild(QCheckBox, "scaleCheckbox")
        self.scaleCheckbox.stateChanged.connect(self.toggle_scale)
        self.scaleCheckbox.setCheckState(Qt.Checked)

        # Chart view
        self.chartview = self.findChild(QChartView)
        self.chartview.setChart(self.chart)
        self.chartview.setRenderHint(QPainter.Antialiasing)

        self.selectionChanged()

    def add_series(self, residue: str):
        if self.model is None:
            raise ValueError("Shiftmap model has not been set")

        series = self.chart.createSeries(residue)
        firstRow = self.model.df.index.levels[0].get_loc(residue)
        mapper = self.chart.createMapper(
            series,
            firstRow=firstRow * self.model.stepCount(),
            rowCount=self.model.stepCount())
        series.clicked.connect(self.seriesSelected)
        self.seriesMap[residue] = series
        self.mapperMap[residue] = mapper

    def selectionChanged(self):
        toggle = self.selectionModel.rowCount() > 0
        if self.model.filter:
            if toggle:
                self.toggle_filter(True)
            else:
                self.toggleBtn.setChecked(False)
        self.controls.setEnabled(toggle)

    def update(self):
        self.seriesMap.clear()
        self.mapperMap.clear()
        self.chart.removeAllSeries()
        self.chart.highlighted = None
        for residue in self.model.df.index.levels[0].difference(
                self.model.shiftumi.incomplete["index"]):
            self.add_series(str(residue))
        self.chart.update_range()

    @pyqtSlot(int)
    def selectResidue(self, residueRow: int):
        index = self.selectionModel.index(residueRow, 0)
        if index.isValid():
            residue = self.selectionModel.data(index)
            series = self.seriesMap[residue]
            self.chart.selectSeries(series)

    @pyqtSlot()
    def seriesSelected(self):
        residue = self.sender().name()
        model = self.selectedList.model()
        indexList = model.match(model.index(0, 0), Qt.DisplayRole, residue)
        selectionModel = self.selectedList.selectionModel()
        if not indexList:
            selectionModel.clear()
        else:
            selectionModel.select(indexList.pop(),
                                  selectionModel.ClearAndSelect)

    @pyqtSlot(bool)
    def toggle_filter(self, toggle: bool):
        QApplication.setOverrideCursor(Qt.WaitCursor)
        for residue, series in self.seriesMap.items():
            if self.model.shiftumi.isSelected(residue):
                series.setVisible(True)
            else:
                series.setVisible(not toggle)
        self.model.toggle_filter(toggle)
        if self.model.scale:
            self.chart.update_range()
        QApplication.restoreOverrideCursor()

    @pyqtSlot(int)
    def toggle_scale(self, toggle: bool):
        self.model.setScale(toggle)
        if self.model.filter:
            self.chart.update_range()
