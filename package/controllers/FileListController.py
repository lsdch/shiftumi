from PyQt5.QtCore import (QObject, QStandardPaths, Qt, QThreadPool, QTimer,
                          pyqtSlot, pyqtSignal, QModelIndex, QFileInfo)
from PyQt5.QtWidgets import (QFileDialog, QHeaderView, QListWidgetItem,
                             QApplication, QListWidget)

from collections import OrderedDict
import qtawesome as qta
import pandas as pd

from package.models.DataFrameModel import DataFrameModel
from package.models.DataFrame3DModel import DataFrame3DModel, RMNDataframeModel
from package.classes.Shiftumi import Shiftumi
from package.classes.Worker import Worker


class FileListController(QObject):

    def __init__(self, window):
        super().__init__(window)
        self.patch_ui()

        # Lists files in experiment
        self.fileList = window.fileListWidget

        # Pandas dataframe model to load tsv files
        self.fileModel = RMNDataframeModel(self)
        self.fileModel.setSourceModel(window.model)

        # Displays file metadata and content as a table
        self.filePreview = window.filePreview
        self.filePreview.setModel(self.fileModel)
        self.filePreview.horizontalHeader().setSectionResizeMode(
            QHeaderView.Stretch)

        self.fileInfo = window.fileInfoLayout

        self.connectEvents()
        self.toggleUI()

    def connectEvents(self):
        # Display file content in table on selection
        self.fileList.currentRowChanged.connect(
            self.display_row)

    def display_row(self, row):
        self.fileModel.set_target_sub_row(row)
        self.fileInfo.display(self.fileList.item(row))

    def toggleUI(self):
        """Enables and disables UI elements
        """
        toggle = len(self.fileList)
        self.filePreview.setEnabled(toggle)
        self.fileInfo.setEnabled(toggle)

    def loadList(self, sourceList: QListWidget):
        """Update list content from file diaog
        """
        self.fileList.clone(sourceList)
        self.toggleUI()

    def patch_ui(self):
        # File dialog button icon
        self.parent().editFilesBtn.setIcon(
            qta.icon("fa.file", "fa.cog",
                     options=[{'color': "lightgreen"},
                              {"scale_factor": 0.6}]))
