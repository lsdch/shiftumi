""" Shiftumi class module

Shiftumi allows manipulating chemical shift data measured with 2D NMR
Input is a set of `.list` tabular files with one residue per line, e.g output
of Sparky.
It calculates chemical shift variation at each titration step using the first
step as reference.
The chemical shifts measured at each residue position are transformed into a
single 'intensity' value.
"""

import numpy as np
import pandas as pd
import yaml

from package.classes.protocole import TitrationProtocole

# ------------------------------------------------------------------------------
# Class Shiftumi
# ------------------------------------------------------------------------------


class Shiftumi(object):
    """
    Class Shiftumi
    Provides methods for accessing each titration step data
    """

    parseOptDefault = {
        "infer": True,
        "header": "infer",
        "columnMap": [0, 1, 2]
    }

    def __init__(self, **kwargs):

        # Input files parsing options
        self.parseOpt = Shiftumi.parseOptDefault

        # Titration protocole
        self.protocole = TitrationProtocole()

        # Initialize app data containers as pandas dataframes
        self.init_dataframes()

        # Cut-off applied to chemical shifts
        self._cutoff = 0
        # List of selected residues
        self.selected = []
        # Incomplete residues to be ignored : e.g. missing value in .list files
        self.incomplete = {
            "rows": [],  # list of int
            "index": pd.Index([])  # pandas Index rows
        }

    # --------------------------------------------------------------------------
    # INITS
    # --------------------------------------------------------------------------

    def init_dataframes(self):
        """Init dataframes containing main app data
        """
        self.mux = pd.MultiIndex.from_product([[], []],
                                              names=("Residue", 'step'))
        self.init_df()
        self.init_intensities()

    def init_df(self, df=None):
        if df is None:
            df = pd.DataFrame([], index=self.mux)
        self._df = df

    def init_intensities(self):
        self.computed = pd.DataFrame([], index=self.mux, columns=["intensity"])

    def init_selection(self):
        self.selected = [False] * len(self.df.index.levels[0])

    # --------------------------------------------------------------------------
    # PROPERTIES
    # --------------------------------------------------------------------------

    @property
    def df(self):
        return self._df

    @property
    def cutoff(self):
        return self._cutoff

    @property
    def stepCount(self):
        if self.df.empty:
            return 0
        return len(self.df.index.unique(level=1))

    # --------------------------------------------------------------------------
    # Titration + RMN Analysis
    # --------------------------------------------------------------------------
    def isSelected(self, residue):
        """
        Returns a boolean indicating if a residue is selected.
        Query by residue index or residue label
        """
        if isinstance(residue, str):
            index = self.df.index.levels[0].get_loc(residue)
        elif isinstance(residue, int):
            index = residue
        else:
            raise ValueError(f"Invalid residue type : {type(residue)}")
        return self.selected[index]

    def set_parse_options(self, options: dict):
        # Use pandas 'infer' flag to parse headers if requested
        options["header"] = "infer" if options["header"] else None
        self.parseOpt = options

    def infer_parse_options(self, path: str):
        "Check input file structure to try and infer columns to parse"

        df = self.load_csv(path)

        parseOpt = Shiftumi.parseOptDefault

        # Check for header line
        parseOpt["header"] = (None if df.columns.dtype == np.float64
                              else "infer")

        # Reload with header option
        df = self.load_csv(path, parseOpt=parseOpt)
        indexType = df.index.dtype
        columnTypes = df.dtypes

        # Check if index column does not seem to contain labels
        if indexType == np.float64:
            columnTypes.reset_index(drop=True, inplace=True)
            # Find non float column that could be the label column
            nonFloatColumns = columnTypes[columnTypes != np.float64]
            for columnNumber in nonFloatColumns.index + 1:
                # Ignore columns containing duplicated values
                # as they are unsuitable for indexing
                if df.iloc[:, 1].duplicated().any():
                    continue
                # Use this column as index for parsed pandas df
                columnPosition = parseOpt["columnMap"].pop(columnNumber)
                parseOpt["columnMap"].insert(0, columnPosition)
                # Reload dataframe with new column layout
                df = self.load_csv(path, parseOpt=parseOpt)
                break
        # Infer proton and nitrogen columns,
        # assuming nitrogen has higher chemical shifts
        if (df.iloc[:, 0] - df.iloc[:, 1]).mean() > 0:
            parseOpt["columnMap"][1:] = reversed(parseOpt["columnMap"][1:])

        return parseOpt

    def validate_df(self, df):
        "Validate df content"

        # Non recoverable errors
        errors = []
        # Recoverable
        warnings = []

        # Dataframe rows must be uniquely indexed
        if not df.index.is_unique:
            errors.append("Index is not unique")
        # All df columns must have type float (not including indexes)
        if not (df.dtypes == np.float64).all():
            errors.append("At least one column has non float type")
        # Invalidate all residues if file contains errors
        if errors:
            incompleteCount = df.index.size
        # If not, check for warnings
        else:
            # Nitrogen chemical shifts are expected to be higher
            # than those of proton
            seriesDiffMean = (df.iloc[:, 1] - df.iloc[:, 0]).mean()
            if seriesDiffMean <= 0:
                warnings.append(
                    "Average proton chemical shift higher than nitrogen")
            # Count residues having chemical shifts = 0
            incompleteCount = (~df.all(axis=1)).sum()
            if incompleteCount:
                warnings.append("Some residues have incomplete data")

        return {
            "warnings": warnings,
            "errors": errors,
            "incompletes": int(incompleteCount),
            "residues": int(df.index.size)
        }

    def load_csv(self, filePath: str, parseOpt=None):
        "Reads a CSV file as an indexed panda dataframe"
        if parseOpt is None:
            parseOpt = self.parseOpt
        with open(filePath, "r") as fh:
            df = pd.read_table(fh, sep='\\s+',
                               header=parseOpt["header"],
                               float_precision="high")
            # Replace NAs with 0
            df.fillna(value=0, inplace=True)
            # Reorder columns
            df = df.reindex(columns=df.columns[parseOpt["columnMap"]])
            df.set_index(df.columns[0], inplace=True)
        return df

    def set_step_dataframe(self, df, step: int):
        "Assign a dataframe (a set of chemical shift data) to a titration step"
        # Insert next step by default
        #   step = step if step is not None else self.stepCount()

        # Set columns name
        df.index.set_names("Residue")
        df.columns = ["Proton", "Nitrogen"]
        # Set step column
        df['step'] = step
        # Index dataframe ; df is now multiindexed with step and residues
        df.set_index(["step"], inplace=True, append=True)
        df.sort_index(inplace=True)
        # Validate dataframe content
        self.validate_df(df)

        # Initialize main dataframe with current df
        if self.df.empty:
            self.init_df(df)
        # Or update main df with current df content
        else:
            # Drop step if exists
            if step in self.df.index.levels[1]:
                self.df.drop(step, level=1, inplace=True)
            # Concat new df with existing 3D df
            self._df = pd.concat([self.df, df]).sort_index()
        return df

    def update_incomplete_residues(self):
        "Find and store incomplete residues row and label from main df"

        resCompleteBool = ~self.df.groupby(level=0).all().all(axis="columns")
        resRow = np.where(resCompleteBool)[0].tolist()
        resIndex = self.df.index.levels[0][resCompleteBool]
        self.incomplete = {
            "rows": resRow,
            "index": resIndex
        }

    def set_selected(self, indicesList: list):
        self.selected = indicesList

    def set_sequence(self, sequence, offset=0):
        "Sets protein aminoacid sequence"
        raise NotImplementedError

    def set_cutoff(self, cutoff: float):
        "Sets cut off for all titration steps"
        self._cutoff = cutoff

# -------------------------
# Utils
# -------------------------

    def computeDeltas(self):
        "Compute chemical shift intensities"
        if self.df.empty:
            self.init_intensities()
        else:
            # Compute chem shift deltas between each steps
            # while ignoring incomplete residues
            deltas = self.df.drop(
                self.incomplete['index'],
                level=0
            ).groupby(level=0).apply(self.deltas)
            # Find incomplete residues
            incompletes = pd.Series(data=0,
                                    index=pd.MultiIndex.from_product(
                                        [self.incomplete['index'],
                                         range(self.stepCount)],
                                        names=["Residue", "step"]))
            # Set incomplete residues intensities to 0
            intensities = (deltas.iloc[:, 0].pow(2) +
                           deltas.iloc[:, 1].div(5).pow(2)
                           ).pow(0.5)
            # Append to complete residues intensities
            intensities = intensities.append(incompletes)
            # Sort index
            self.computed = intensities.to_frame("intensity").sort_index()

    def deltas(self, subdf):
        "Computes deltas per residue on a given step"
        # Reference data at step 0
        reference = subdf.iloc[0, :]
        # Delta
        deltas = subdf.sub(reference.values, axis=1)
        return deltas
