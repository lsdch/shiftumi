from PyQt5.QtChart import QChart, QLineSeries, QValueAxis, QVXYModelMapper
from PyQt5.QtCore import QMargins, Qt, pyqtSlot
from PyQt5.QtGui import QColor, QPainter


class ConcentrationChart(QChart):

    def __init__(self, model, chartView):
        super().__init__()

        # Create series
        self.init_series()
        # Init chart axis and attach series
        self.init_axis(model)
        # Init model mappers
        self.init_mappers(model)

        # Attach to chart to view
        chartView.setChart(self)
        chartView.setRenderHint(QPainter.Antialiasing)
        self.layout().setContentsMargins(2, 2, 2, 2)
        self.setMargins(QMargins(5, 5, 5, 5))

    def init_series(self):
        # Titrant concentration series
        self.titrantSeries = QLineSeries()
        self.titrantSeries.setName("[titrant]")
        self.titrantSeries.setPointsVisible()
        # Analyte concentration series
        self.analyteSeries = QLineSeries()
        self.analyteSeries.setName("[analyte]")
        pen = self.analyteSeries.pen()
        pen.setColor(QColor("orange"))
        pen.setWidth(2)
        self.analyteSeries.setPen(pen)
        self.analyteSeries.setPointsVisible()
        # Ratio series
        self.ratioSeries = QLineSeries()
        self.ratioSeries.setName("Ratio")
        pen = self.ratioSeries.pen()
        color = QColor("#99CA53")
        color.setAlphaF(0.8)
        pen.setColor(color)
        pen.setWidth(2)
        pen.setStyle(Qt.DotLine)
        self.ratioSeries.setPen(pen)

        # Add both series
        self.addSeries(self.titrantSeries)
        self.addSeries(self.analyteSeries)
        self.addSeries(self.ratioSeries)

    def init_mappers(self, model):
        self.titrantMapper = QVXYModelMapper(self)
        self.titrantMapper.setXColumn(6)  # X column : STEP
        self.titrantMapper.setYColumn(3)  # Y column : titrant CONC
        self.titrantMapper.setSeries(self.titrantSeries)  # set series
        self.titrantMapper.setModel(model)  # set model

        self.analyteMapper = QVXYModelMapper(self)
        self.analyteMapper.setXColumn(6)  # X column : STEP
        self.analyteMapper.setYColumn(4)  # Y column : analyte CONC
        self.analyteMapper.setSeries(self.analyteSeries)  # set series
        self.analyteMapper.setModel(model)  # set model

        self.ratioMapper = QVXYModelMapper(self)
        self.ratioMapper.setXColumn(6)  # X column : STEP
        self.ratioMapper.setYColumn(5)  # Y column : ratio
        self.ratioMapper.setSeries(self.ratioSeries)  # set series
        self.ratioMapper.setModel(model)  # set model

    def init_axis(self, model):
        # X Axis
        stepAxis = QValueAxis()
        stepAxis.setTickCount(model.rowCount() + 1)
        stepAxis.setTitleText("Step")
        stepAxis.setLabelFormat("%i")
        self.setAxisX(stepAxis, self.titrantSeries)

        # Y Axis
        self.concAxis = QValueAxis()
        self.concAxis.setTitleText("Concentration (µM)")
        self.addAxis(self.concAxis, Qt.AlignLeft)

        # Ratio axis
        self.ratioAxis = QValueAxis()
        self.ratioAxis.setTitleText("Ratio")
        pen = self.ratioAxis.gridLinePen()
        pen.setStyle(Qt.DotLine)
        pen.setColor(QColor(0, 0, 0, 60))
        self.ratioAxis.setGridLinePen(pen)
        self.addAxis(self.ratioAxis, Qt.AlignRight)

        # Attach them to both axis
        self.titrantSeries.attachAxis(self.concAxis)
        self.analyteSeries.attachAxis(stepAxis)
        self.analyteSeries.attachAxis(self.concAxis)
        self.ratioSeries.attachAxis(stepAxis)
        self.ratioSeries.attachAxis(self.ratioAxis)

    @pyqtSlot(int, float, float)
    def update_axis(self, rowCount, maxConcentration, maxRatio):
        self.axisX().setRange(0, rowCount - 1)
        self.axisX().setTickCount(rowCount)

        self.concAxis.setRange(0, maxConcentration)
        self.concAxis.setTickCount(6)
        self.concAxis.applyNiceNumbers()

        self.ratioAxis.setRange(0, maxRatio)
        self.ratioAxis.setTickCount(6)
        self.ratioAxis.applyNiceNumbers()
