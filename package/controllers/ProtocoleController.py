import qtawesome as qta
from PyQt5 import uic
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QWidget

from package.classes.protocole import TitrationProtocole
from package.models.ProtocoleModel import ProtocoleModel
from package.widgets.charts.ConcentrationChart import ConcentrationChart
from package.widgets.charts.VolumeChart import VolumeChart


class ProtocoleController(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi("designer/protocole.ui", self)

        self.addStepBtn.setIcon(qta.icon('fa.plus'))
        self.removeStepBtn.setIcon(qta.icon('fa.minus'))
        self.trimStepsBtn.setIcon(qta.icon('fa.eraser'))

        self.addStepBtn.clicked.connect(self.add_step)
        self.removeStepBtn.clicked.connect(self.remove_step)
        self.trimStepsBtn.clicked.connect(self.trim_steps)

        self.initialVolumes.updated.connect(self.refresh)
        self.analyte.updated.connect(self.refresh)
        self.titrant.updated.connect(self.refresh)

    def set_protocole(self, protocole):
        """
        Set Protocole backend and initialize attached model
        """
        self.protocole = protocole
        self.protocole.update()
        self.model = ProtocoleModel(self.protocole, parent=self)
        for section, value in enumerate(self.headers):
            self.model.setHeaderData(section, Qt.Horizontal, value)

        self.model.removeDisabled.connect(self.toggleRemove)

        # Attach model to internal widgets
        self.table.setModel(self.model)
        self.init_chart()
        self.analyte.set_model(self.protocole.analyte)
        self.titrant.set_model(self.protocole.titrant)
        self.initialVolumes.set_protocole(self.protocole)

    @pyqtSlot(list)
    def update_file_list(self, fileList: list):
        self.model.set_file_list(fileList)

    def init_chart(self):
        # Init charts
        self.volumeChart = VolumeChart(self.model, self.volumeChartView)
        self.concChart = ConcentrationChart(self.model, self.concChartView)

        self.model.volumesChanged.connect(self.volumeChart.update_axis)
        self.model.concentrationsChanged.connect(self.concChart.update_axis)

    @property
    def headers(self):
        titrant = self.protocole.titrant.name
        analyte = self.protocole.analyte.name

        return [
            f'Added\n{titrant} (µL)',
            f'Total\n{titrant} (µL)',
            f'Total\nvolume (µL)',
            f'[{titrant}] (µM)',
            f'[{analyte}] (µM)',
            f'Ratio'
        ]

    @pyqtSlot()
    def refresh(self):
        self.model.refresh()

    @pyqtSlot()
    def add_step(self):
        self.model.add_step()

    @pyqtSlot()
    def trim_steps(self):
        while self.rowCount > self.minSteps:
            self.remove_step()

    @pyqtSlot()
    def remove_step(self):
        self.model.remove_step(self.table.currentIndex())
        return True

    @property
    def rowCount(self):
        return self.model.rowCount()

    @pyqtSlot(list)
    def file_list_changed(self, files):
        self.model.set_file_list(files)

    @pyqtSlot(bool)
    def toggleRemove(self, toggle):
        self.removeStepBtn.setDisabled(toggle)
        self.trimStepsBtn.setDisabled(toggle)

    @pyqtSlot(list)
    def set_min_steps(self, steps):
        self.model.minSteps = max(1, steps)
        while self.rowCount < self.minSteps:
            self.add_step()

    @property
    def minSteps(self):
        return self.model.minSteps

    @property
    def rowCount(self):
        return self.model.rowCount()
